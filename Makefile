all: royalteazer.pdf covers/covers.pdf

royalteazer.pdf: *.crd
	chordpro *.crd --output royalteazer.pdf

covers/covers.pdf: covers/*.crd
	chordpro covers/*.crd --output covers/covers.pdf
